import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from 'react-router-dom';

import CreateEmpForm from './components/createEmpForm';
import EmpList from './components/empList';
import EditEmpForm from './components/editEmpForm';
import PersonForm from './components/functionFile';

function App() {
  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navheader">
        <div className="collapse navbar-collapse" >
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={'/empList'} className="nav-link">Employee List</Link>
            </li>
            <li className="nav-item">
              <Link to={'/empForm'} className="nav-link">Employee Form</Link>
            </li>
            <li className="nav-item">
              <Link to={'/function'} className="nav-link">Function Form</Link>
            </li>
          </ul>
        </div>
      </nav> <br />
      <Switch>
        <Route exact path="/" Component={EmpList}></Route>
        <Route path="/empList" component={EmpList}></Route>
        <Route path="/empForm" component={CreateEmpForm}></Route>
        <Route path="/editEmpForm" component={EditEmpForm}></Route>
        <Route path="/function" component={PersonForm}></Route>
      </Switch> 
    </div>
  );
}
export default App;
