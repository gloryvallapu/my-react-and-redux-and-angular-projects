import React, { useState } from 'react';
import axios from 'axios';
import { Container, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const PersonForm = (props) => {

    let [account, setAccount] = useState({
        name: '',
        email: '',
        mobile: ''
    });

    let handleChnage = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        account[name] = value;
        setAccount(account);
    }

    let onSubmitData = (e) => {
        e.preventDefault();
        axios.post('http://localhost:3000/api/users/createEmployeeForm', account)
            .then(res => {
                if (res.data.success === 1) {
                    alert(res.data.message);
                    props.history.push('/empList')
                }
            })
    }

    return (

        <Container className="App">

            <h4 className="PageHeading">Person Informations</h4>
            <Form className="form" method="post" onSubmit={onSubmitData}>
                <Col>
                    <FormGroup row>
                        <Label for="name" sm={2}>First Name</Label>
                        <Col sm={10}>
                            <Input type="text" name="name" onChange={handleChnage}
                                placeholder="Enter name" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="email" sm={2}>Email</Label>
                        <Col sm={10}>
                            <Input type="text" name="email" onChange={handleChnage} placeholder="Enter Email" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="mobile" sm={2}>Mobile</Label>
                        <Col sm={10}>
                            <Input type="text" name="mobile" onChange={handleChnage} placeholder="Enter mobile" />
                        </Col>
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup row>
                        <Col sm={5}>
                        </Col>
                        <Col sm={1}>
                            <Button type="submit" color="success">Submit</Button>{' '}
                        </Col>
                        <Col sm={1}>
                            <Link to="/View">
                                <Button color="danger">Cancel</Button>
                            </Link>{' '}
                        </Col>
                        <Col sm={5}>
                        </Col>
                    </FormGroup>
                </Col>
            </Form>

        </Container>
    );
}
export default PersonForm;




// import React, {useEffect, useState} from 'react';

//  const EffectDemo = () => {
//     //State
//     const [fullName, setFullName] = useState({name: 'name', familyName: 'family'});
//     const [title,setTitle] = useState('useEffect() in Hooks');

//     //useEffect
//     useEffect(() => {
//         console.log('useEffect has been called!');
//         setFullName({name:'Marco',familyName: 'Shaw'});
//     },[]); //Pass Array as second argument

//     return(
//         <div>
//             <h1>Title: {title}</h1>
//             <h3>Name: {fullName.name}</h3>
//             <h3>Family Name: {fullName.familyName}</h3>
//         </div>
//     );
// };
// export default EffectDemo;