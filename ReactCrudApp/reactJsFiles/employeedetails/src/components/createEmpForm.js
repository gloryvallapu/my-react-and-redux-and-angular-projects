import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, Col, Container } from 'reactstrap';

class CreateEmpForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            email: "",
            mobile: ""
        }
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeMobile = this.handleChangeMobile.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }

    handleChangeName(e) {
        this.setState({ name: e.target.value })
    }
    handleChangeEmail(e) {
        this.setState({ email: e.target.value })
    }
    handleChangeMobile(e) {
        this.setState({ mobile: e.target.value })
    }

    formSubmit(e) {
        e.preventDefault();
        console.log("form submitted");

        const data = {
            name: this.state.name,
            email: this.state.email,
            mobile: this.state.mobile,
        }

        axios.post('http://localhost:3000/api/users/createEmployeeForm', data)
            .then(res => {
                if (res.data.success === 1)
                    alert(res.data.message);
                this.props.history.push('/empList');
            })
    }

    render() {
        return (
            <Container>
                <br />
                <h4 className="txt-center">Employee Form</h4>
                <Form onSubmit={this.formSubmit} ref="form">
                    <Col>
                        <FormGroup row>
                            <Label for="name">Name</Label>
                            <Col sm={10}>
                                <Input type="name" name="name" id="name" placeholder="Enter your Name" value={this.state.name} onChange={this.handleChangeName} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="email">Email</Label>
                            <Col sm={10}>
                                <Input type="email" name="email" id="email" placeholder="Enter your Email-ID" value={this.state.email} onChange={this.handleChangeEmail} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="mobile">Mobile</Label>
                            <Col sm={10}>
                                <Input type="mobile" name="mobile" id="mobile" placeholder="Enter your Mobile" value={this.state.mobile} onChange={this.handleChangeMobile} />
                            </Col>
                        </FormGroup>
                        <div className="txt-center">
                            <Button type="submit" color="success">Submit</Button>{' '}
                            <Button type="submit" color="danger">Cancel</Button>
                        </div>
                    </Col>
                </Form>
            </Container>
        )
    }
}
export default CreateEmpForm;