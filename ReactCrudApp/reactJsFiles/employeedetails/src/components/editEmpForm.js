import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, Col, Container } from 'reactstrap';

class EditEmpForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            email: "",
            mobile: ""
        }
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeMobile = this.handleChangeMobile.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }

    handleChangeName(e) {
        this.setState({ name: e.target.value })
    }
    handleChangeEmail(e) {
        this.setState({ email: e.target.value })
    }
    handleChangeMobile(e) {
        this.setState({ mobile: e.target.value })
    }

    formSubmit(e) {
        e.preventDefault();
        const data = {
            name: this.state.name,
            email: this.state.email,
            mobile: this.state.mobile,
            id: this.props.location.empList.id
        }
        axios.put('http://localhost:3000/api/users/updateEmp', data)
            .then(res => {
                if (res.data.success === 1)
                    alert(res.data.message);
                this.props.history.push('/empList');
            })
    }

    componentDidMount() {
        console.log(this.props);
        const editData = this.props.location.empList;
        console.log("editData", editData);
        this.setState({
            name: editData.name,
            email: editData.email,
            mobile: editData.mobile
        })
    }

    render() {
        return (
            <Container>
                <br />
                <h4>Employee Form</h4>
                <Form onSubmit={this.formSubmit} ref="form">
                    <Col>
                        <FormGroup row>
                            <Label for="name">Name</Label>
                            <Col sm={10}>
                                <Input type="name" name="name" id="name" placeholder="Enter your Name" value={this.state.name} onChange={this.handleChangeName} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="email">Email</Label>
                            <Col sm={10}>
                                <Input type="email" name="email" id="email" placeholder="Enter your Email-ID" value={this.state.email} onChange={this.handleChangeEmail} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="mobile">Mobile</Label>
                            <Col sm={10}>
                                <Input type="mobile" name="mobile" id="mobile" placeholder="Enter your Mobile" value={this.state.mobile} onChange={this.handleChangeMobile} />
                            </Col>
                        </FormGroup>
                        <Button type="submit">Update</Button>
                    </Col>
                </Form>
            </Container>
        )
    }
}
export default EditEmpForm;