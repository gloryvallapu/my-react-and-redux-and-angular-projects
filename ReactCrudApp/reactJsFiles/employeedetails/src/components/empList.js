import React, { Component } from 'react';
import axios from 'axios';
import { Table, Container, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

class EmpList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            empList: [],
        }
    }

    componentDidMount() {
        this.getEmpList();
    }

    getEmpList() {
        axios.get('http://localhost:3000/api/users/getEmpList')
            .then((res) => {
                // console.log("result is", res.data)
                if (res.data.data.success === 1)
                    alert(res.data.data.message);
                this.setState({
                    empList: res.data.data
                })
            });
    }

    deleteEmp(id) {
        axios.delete('http://localhost:3000/api/users/deleteEmp/' + id)
            .then((res) => {
                console.log("result is", res.data)
                if (res.data.success === 1)
                    alert(res.data.message);
                this.getEmpList();
            });
    }

    render() {
        return (
            <Container>
                <br />
                <h4>Employee List</h4>
                <Table striped>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.empList.map((list) => (
                            <tr key={list.id}>
                                <th>{list.id}</th>
                                <td>{list.name}</td>
                                <td>{list.email}</td>
                                <td>{list.mobile}</td>
                                <td>
                                    <Link to={{ pathname: '/editEmpForm', empList: list }}>
                                        <Button color="info">Edit</Button>
                                    </Link>{' '}
                                    <Button onClick={() => this.deleteEmp(list.id)} color="danger">Delete</Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </Container>
        )
    }
}
export default EmpList;