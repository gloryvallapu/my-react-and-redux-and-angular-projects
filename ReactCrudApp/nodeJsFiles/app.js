require('dotenv').config();
const cors = require('cors');
const express = require('express');
const app = express();
app.use(cors());
app.options('*', cors());
const userRouter = require('./api/users/user.router');

app.use(express.json());

app.use('/api/users', userRouter);

app.listen(process.env.APP_PORT, () => {
    console.log('server is running at port:', process.env.APP_PORT);
});