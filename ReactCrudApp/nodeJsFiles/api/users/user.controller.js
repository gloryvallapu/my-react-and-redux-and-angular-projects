const {
    create,
    getAllEmployeesWithId,
    getAllEmployees,
    updateEmployee,
    deleteEmployee,
    employeeLogin,
    createProduct,
    getAllProducts
} = require('./user.service');

const bcrypt = require('bcrypt');
const {
    genSaltSync,
    hashSync,
    compareSync
} = require('bcrypt');
const {
    sign
} = require('jsonwebtoken');

module.exports = {
    createUser: (req, res) => {
        const body = req.body;
        // const salt = genSaltSync(10);
        // body.password = hashSync(body.password, salt);
        create(body, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'internal server error'
                });
            }
            return res.status(200).json({
                success: 1,
                message: 'New Record Registered Successfully',
                data: results
            });
        });
    },

    getEmpWithId: (req, res) => {
        const id = req.params.id;
        getAllEmployeesWithId(id, (err, results) => {
            if (err) {
                console.log(err);
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: 'Record not found'
                })
            }
            return res.json({
                success: 1,
                data: results,
                message: 'Record Added Successfully'
            });
        });
    },

    getEmployees: (req, res) => {
        getAllEmployees((err, results) => {
            if (err) {
                console.log(err);
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },

    updateEmp: (req, res) => {
        const body = req.body;
        console.log('body', body)
        // const salt = genSaltSync(10);
        // body.password = hashSync(body.password, salt);
        updateEmployee(body, (err, results) => {
            if (err) {
                console.log(err);
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: 'Failed to update record'
                });
            }
            return res.json({
                success: 1,
                message: 'updated successfully'
            });
        });
    },

    deleteEmp: (req, res) => {
        const body = req.params.id;
        console.log(req.params.id, 'nodereq');
        deleteEmployee(body, (err, results) => {
            console.log(results);
            if (err) {
                console.log(err);
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: 'Record not found'
                });
            }
            return res.json({
                success: 1,
                message: 'Employee Data Deleted successfully'
            });
        });
    },

    login: (req, res) => {
        const body = req.body;
        console.log(req.body)
        // const salt = genSaltSync(10);
        // body.password = hashSync(body.password, salt);
        employeeLogin(body.email, (err, results) => {

            if (err) {
                console.log(err)
            }
            if (!results) {
                return res.json({
                    success: 0,
                    message: 'Invalid Email or Password'
                });
            }
            // const result =true;
            const result = compareSync(body.password, results.password);
            if (result) {
                console.log("result", result);
                results.password = undefined;
                const jsontoken = sign({
                    result: results
                }, 'qwe1234', {
                    expiresIn: '1h'
                });
                return res.json({
                    success: 1,
                    message: 'Login successfully',
                    body: results,
                    token: jsontoken
                });
            } else {
                return res.json({
                    success: 0,
                    message: 'Invalid email or Password'
                });
            }
        });
    },

    //product_list table queries

    createProductList: (req, res) => {
        const body = req.body;
        createProduct(body, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    success: 0,
                    message: 'internal server error'
                });
            }
            return res.status(200).json({
                success: 1,
                message: 'Product Added Successfully',
                data: results
            });
        });
    },

    getProducts: (req, res) => {
        getAllProducts((err, results) => {
            if (err) {
                console.log(err);
            }
            return res.json({
                success: 1,
                data: results
            });
        });
    },


}