const { createUser,
    getEmpWithId,
    getEmployees,
    updateEmp,
    deleteEmp,
    login,
    createProductList,
    getProducts
} = require('./user.controller');
const router = require('express').Router();

//const { checkToken } = require('../../auth/token_validation');

// router.post('/signup', createUser);
// router.get('/getList', getEmployees);
router.get('empListWithID/:id', getEmpWithId);
// router.put('/updateEmp', updateEmp);
// router.delete('/delete/:id', deleteEmp);
// router.post('/addProduct', createProductList);
// router.post('/getProductsList', getProducts);
// router.post('/login', login);

router.post('/createEmployeeForm', createUser);
router.get('/getEmpList', getEmployees);
router.delete('/deleteEmp/:id', deleteEmp);
router.put('/updateEmp', updateEmp);

module.exports = router;