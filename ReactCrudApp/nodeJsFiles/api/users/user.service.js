const pool = require('../../config/database');

module.exports = {
    create: (data, callback) => {
        var employee={
                "name":data.name,
                "email":data.email,
                "mobile":data.mobile
        }
        pool.query('INSERT INTO employee SET ?', employee, (error, results, feilds) => {
                if(error){
                    return callback(error);
                }
                return callback(null, results);
            });
    },

    getAllEmployees: (callback) =>{
        pool.query('select * from employee', [], (error, results, feilds) => {
            if(error){
                return callback(error);
            }
            return callback(null, results);
        });
    },

    getAllEmployeesWithId:(id, callback)=>{
        pool.query('select * from employee where id=?', [id], (error, results, feilds)=>{
            if(error){
                return callback(error);
            }
            return callback(null, results[0]);
        });
    },

    updateEmployee:(data, callback)=>{
        pool.query('update employee set name=?, email=?, mobile=? where id=?',
        [
            data.name,
            data.email,
            data.mobile,
            data.id
        ],
        (error, results, feilds)=>{
            if(error){
                return callback(error);
            }
                return callback(null, results);
        });  
    },

    deleteEmployee:(data, callback)=>{
        // console.log(data , 'node delete');
        pool.query('delete from employee where id=?', [data], (error, results, feilds)=>{
            if(error){
                return callback(error);
            }
            console.log(results,'result')
            return callback(null, results);
            
        });
    },

    employeeLogin:(email, callback)=>{
        console.log(email)
       pool.query('select * from employeeReg where email=?', [email], (error, results, feilds)=>{
            if(error){
                return callback(error);
            }
            console.log(results)
            return callback(null, results[0]);
        });
    },

    // product_list table queries

    createProduct: (data, callback) => {
        var product={
                "product_name":data.product_name,
                "sku":data.sku,
                "price":data.price,
                "id":data.id
        }
        console.log(product);
        pool.query('INSERT INTO products SET ?', product, (error, results, feilds) => {
                if(error){
                    return callback(error);
                }
                return callback(null, results);
            });
    },

    getAllProducts: (callback) =>{
        pool.query('select * from products', [], (error, results, feilds) => {
            if(error){
                return callback(error);
            }
            return callback(null, results);
        });
    },

}